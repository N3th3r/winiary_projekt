<?php
    include "get_user_data.php";

    const PRIVILAGE_LIST = Array(
        "User_privileges" => 0,
        "Forum_moderation" => 1,
        "Article_moderation" => 2,
        "Privileges_edit" => 3,
        "User_moderation" => 4,
        "Admin_panel" => 5,
        "Protected" => 6
    );

    function check_privileges($check_type, ...$check_for)
    {
        if(isset($_SESSION["user"]))
        {
            $user_permissions = sprintf( "%011d", decbin($_SESSION["user"]["permissions"]));
            if($check_type == "ANY")
            {
                $privilege_sum = 0;
                foreach($check_for as $permission)
                {
                    $privilege_sum += $user_permissions[PRIVILAGE_LIST[$permission]];
                }    
            }
            elseif($check_type == "ALL")
            {
                $privilege_sum = 1;
                foreach($check_for as $permission)
                {
                    $privilege_sum *= $user_permissions[PRIVILAGE_LIST[$permission]];
                }    
            }
            return boolval($privilege_sum);
        }
        return false;
    }

    function set_privileges($user_id, ...$privileges)
    {
        $con = mysqli_connect('localhost', 'root', '') or die('Wystąpił błąd' . mysqli_error($con));	
        mysqli_select_db($con, 'fishing') or die('Wystąpił błąd' . mysqli_error($con));
        $stmt = $con->prepare("SELECT permissions FROM users WHERE id = ?");
        $stmt->bind_param('s', $user_id);
        $stmt->execute() or die('Wystąpił błąd' . mysqli_error($link));
        $stmt->bind_result($permissions);
        $stmt->fetch();
        $stmt->close();

        $check_priv = translate_permission_sum($permissions);
        
        if(isset($_SESSION["user"]) && check_privileges("ALL", "User_moderation", "Privileges_edit") && !in_array("Protected", $check_priv))
        {
            $privilege_sum = 0;
            $permission_field_len = 11;
            foreach($privileges as $privilege)
            {
                $privilege_sum += 2**($permission_field_len - PRIVILAGE_LIST[$privilege] - 1);
            }

            $stmt = $con->prepare("UPDATE users SET permissions = ? WHERE id = ?");
            $stmt->bind_param('is', $privilege_sum, $user_id);
            $stmt->execute() or die('Wystąpił błąd' . mysqli_error($link));
        }
        $con->close();
    }

    function get_privileges()
    {
        $user_permissions = sprintf( "%011d", decbin($_SESSION["user"]["permissions"]));
        $privilege_array = array();
        for($i = 0; $i <= strlen($user_permissions); $i++)
        {
            if(in_array($i, PRIVILAGE_LIST) && $user_permissions[$i] == 1)
            {
                array_push($privilege_array, array_search($i, PRIVILAGE_LIST));
            }
        }
        return $privilege_array;
    }

    function translate_permission_sum($sum)
    {
        $user_permissions = sprintf( "%011d", decbin($sum));
        $privilege_array = array();
        for($i = 0; $i <= strlen($user_permissions); $i++)
        {
            if(in_array($i, PRIVILAGE_LIST) && $user_permissions[$i] == 1)
            {
                array_push($privilege_array, array_search($i, PRIVILAGE_LIST));
            }
        }
        return $privilege_array;         
    }
?>