<?php
    include "permissions.php";

    session_start();

    if(check_privileges("ALL", "Admin_panel", "User_moderation", "Privileges_edit"))
    {
        if(isset($_GET["id"]))
        {
            set_privileges($_GET["id"], ...$_POST["set_privileges"]);
        }
    }
    header("Refresh:0; url=" . $_SERVER['HTTP_REFERER']);     
?>