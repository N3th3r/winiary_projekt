<?php
    include "permissions.php";

    session_start();
    if(isset($_SESSION['user']))
    {
        $data = array("raw" => $_SESSION['user'], "readable" => get_privileges());
        echo json_encode($data);
    }
    else
    {
        echo json_encode(null);
    }
?>