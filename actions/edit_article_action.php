<?php
    include "permissions.php";

    function is_image($file)
    {
        $uploadOk = 0;
        $check = getimagesize($file);
        if($check !== false) {
          $uploadOk = 1;
        }
        return $uploadOk;
    }

    function upload_files($input, $max_files, $upload_dir)
    {
        $files_count = count($_FILES[$input]['name']);
        if($files_count > 0 && $files_count <= $max_files)
        {
            if (!file_exists($upload_dir)) 
            {
                mkdir($upload_dir, 0777, true);
            }        
            for($i = 0; $i <= $files_count; $i++)
            {
                if(is_image($_FILES[$input]['tmp_name'][$i]))
                {
                    $upload_file = $upload_dir . $_FILES[$input]['name'][$i];
                    move_uploaded_file($_FILES[$input]["tmp_name"][$i], $upload_file);
                }
            }
        }
        else
        {
            echo "To many files selected";
        }    
    }

    function upload_thumbnail($input, $upload_dir)
    {
        if (!file_exists($upload_dir)) 
        {
            mkdir($upload_dir, 0777, true);
        }        
        if(is_image($_FILES[$input]['tmp_name']))
        {
            $upload_file = $upload_dir . "thumbnail.jpg";
            move_uploaded_file($_FILES[$input]["tmp_name"], $upload_file);
        }
    }

    function create_article($link, $article_id)
    {
        if(!empty(trim($_POST["article_title"])) && !empty(trim($_POST["article_text"])))
        {
            $article_title = htmlspecialchars($_POST["article_title"]);
            $article_text = htmlspecialchars($_POST["article_text"]);
            $mod_date = date("Y-m-d H:i:s");
            $user_id = $_SESSION["user"]["id"];
            $stmt = $link->prepare("INSERT INTO articles VALUES (DEFAULT, ?, ?, ?, ?)");
			$stmt->bind_param('ssss', $user_id, $mod_date, $article_title, $article_text);
            $stmt->execute() or die('Wystąpił błąd' . mysqli_error($link));
            upload_thumbnail("thumbnail", "../uploads/articles/" . $article_id . "/");
            upload_files("attachments", 10, "../uploads/articles/" . $article_id . "/");

        }
        else
        {
            echo "article text and title cannot be empty";
        }
    }

    function edit_article($link, $article_id)
    {
        if(!empty(trim($_POST["article_title"])) && !empty(trim($_POST["article_text"])))
        {
            $article_title = htmlspecialchars($_POST["article_title"]);
            $article_text = htmlspecialchars($_POST["article_text"]);
            $mod_date = date("Y-m-d H:i:s");
            $user_id = $_SESSION["user"]["id"];
            $stmt = $link->prepare("UPDATE articles SET modified_by = ?, mod_date = ?, title = ?, text = ? WHERE id = ?");
			$stmt->bind_param('ssssi', $user_id, $mod_date, $article_title, $article_text, $article_id);
            $stmt->execute() or die('Wystąpił błąd' . mysqli_error($link));
        }
        else
        {
            echo "article text and title cannot be empty";
        }        
    }

    session_start();

    if(check_privileges('ALL', 'Article_moderation'))
    {    
        $con = mysqli_connect('localhost', 'root', '') or die('Wystąpił błąd' . mysqli_error($con));
        mysqli_select_db($con, 'fishing') or die('Wystąpił błąd' . mysqli_error($con));
        if(isset($_GET["id"]))
        {
            edit_article($con, $_GET["id"]);
            header("Refresh:0; url=../article.php?id=" . $_GET["id"]);
        }
        else
        {
            $stmt = $con->prepare("SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_NAME = 'articles'");
            $stmt->execute() or die('Wystąpił błąd' . mysqli_error($link)); 
            $stmt->bind_result($article_id);
            $stmt->fetch();
            $stmt->close();
            create_article($con, $article_id);
            header("Refresh:0; url=../article.php?id=" . $article_id);
        }
    }
    else
    {
        header("Refresh:0; url=../index.php");
    }
?>

