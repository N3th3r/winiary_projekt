<?php
    function is_image($file)
    {
        $uploadOk = 0;
        $check = getimagesize($file);
        if($check !== false) {
          $uploadOk = 1;
        }
        return $uploadOk;
    }

    function upload_files($input, $max_files, $upload_dir)
    {
        $files_count = count($_FILES[$input]['name']);
        if($files_count > 0 && $files_count <= $max_files)
        {
            if (!file_exists($upload_dir)) 
            {
                mkdir($upload_dir, 0777, true);
            }        
            for($i = 0; $i <= $files_count; $i++)
            {
                if(is_image($_FILES[$input]['tmp_name'][$i]))
                {
                    $upload_file = $upload_dir . $_FILES[$input]['name'][$i];
                    move_uploaded_file($_FILES[$input]["tmp_name"][$i], $upload_file);
                }
            }
        }
        else
        {
            echo "To many files selected";
        }    
    }

    function create_post($link, $section, $post_id)
    {
        if(!empty(trim($_POST["post_title"])) && !empty(trim($_POST["post_text"])))
        {
            $post_title = htmlspecialchars($_POST["post_title"]);
            $post_text = htmlspecialchars($_POST["post_text"]);
            $creation_date = date("Y-m-d H:i:s");
            $user_id = $_SESSION["user"]["id"];
            if($_GET["thread_id"] == -1)
            {
                $thread_id = $post_id;
            }
            else
            {
                $thread_id = $_GET["thread_id"];
            }
            $stmt = $link->prepare("INSERT INTO posts VALUES (DEFAULT, ?, ?, ?, ?, ?, ?, ?)");
			$stmt->bind_param('sssssii', $post_title, $post_text, $creation_date, $user_id, $section, $_GET["response_to"], $thread_id);
            $stmt->execute() or die('Wystąpił błąd' . mysqli_error($link));
            upload_files("attachments", 10, "../uploads/posts/" . $post_id . "/");
            if($_GET["response_to"] == -1)
            {
                header("Refresh:0; url=../post.php?id=" . $post_id);
            }
            else
            {
                header("Refresh:0; url=../post.php?id=" . $_GET["thread_id"] . "#post_" . $post_id);
            }
        }
        else
        {
            echo "post text and title cannot be empty";
        }
    }
    session_start();

    $con = mysqli_connect('localhost', 'root', '') or die('Wystąpił błąd' . mysqli_error($con));
    mysqli_select_db($con, 'fishing') or die('Wystąpił błąd' . mysqli_error($con));
    $stmt = $con->prepare("SELECT AUTO_INCREMENT FROM information_schema.TABLES WHERE TABLE_NAME = 'posts'");
    $stmt->execute() or die('Wystąpił błąd' . mysqli_error($link)); 
    $stmt->bind_result($post_id);
    $stmt->fetch();
    $stmt->close();
    $upload_dir = "../uploads/posts/" . $post_id . "/";    
    if(isset($_GET["section"]))
    {
        create_post($con, $_GET["section"], $post_id);
    }
?>

