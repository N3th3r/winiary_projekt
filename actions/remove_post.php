<?php
    include "permissions.php";

    function remove_post($id)
    {
            $con = mysqli_connect('localhost', 'root', '') or die('Wystąpił błąd' . mysqli_error($con));	
            mysqli_select_db($con, 'fishing') or die('Wystąpił błąd' . mysqli_error($con));
            $stmt = $con->prepare("DELETE FROM posts WHERE ? IN(id, thread_id, response_to)");
            $stmt->bind_param('i', $id);
            $stmt->execute() or die('Wystąpił błąd' . mysqli_error($con));
            $stmt->close();

            $stmt = $con->prepare("DELETE FROM reported WHERE post_id=?");
            $stmt->bind_param('i', $id);
            $stmt->execute() or die('Wystąpił błąd' . mysqli_error($con));
            $con->close();
    }

    session_start();

    if(check_privileges('ALL', 'Forum_moderation', 'Admin_panel'))
    {
        if(isset($_GET["id"]))
        {
            remove_post($_GET["id"]);
        }
    }
    header("Refresh:0; url=" . $_SERVER['HTTP_REFERER']);        
?>