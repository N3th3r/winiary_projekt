<?php
    function is_image($input_name)
    {
        $uploadOk = 0;
        $check = getimagesize($_FILES[$input_name]["tmp_name"]);
        if($check !== false) {
          $uploadOk = 1;
        }
        return $uploadOk;
    }

    function update_user_data($link)
    {
        $user_id = $_SESSION["user"]["id"];
        $bio =  htmlspecialchars($_POST["bio_edit"]);
        
        if (file_exists($_FILES['profile_img_up']["tmp_name"]))
        {
            if(is_image('profile_img_up'))
            {
                $img_blob = file_get_contents($_FILES['profile_img_up']['tmp_name']);

                $stmt = $link->prepare("UPDATE user_data SET bio = ?, profile_img = ? WHERE id = ?");
                $stmt->bind_param('sss', $bio, $img_blob, $user_id);
    
            }
            else
            {
                header("Refresh:0; url=../edit_account.php?success=0");
            }
        }
        else
        {
            $stmt = $link->prepare("UPDATE user_data SET bio = ? WHERE id = ?");
            $stmt->bind_param('ss', $bio, $user_id);
        }
        $stmt->execute() or die('Wystąpił błąd' . mysqli_error($link));
        header("Refresh:0; url=../account.php?id=" . $_SESSION["user"]["id"]);
    }

    session_start();
 
    if(isset($_POST['cancel_btn']))
    {
        header("Refresh:0; url=../account.php?id=" . $_SESSION["user"]["id"]);
    }
    else
    {
        $con = mysqli_connect('localhost', 'root', '') or die('Wystąpił błąd' . mysqli_error($con));
        mysqli_select_db($con, 'fishing') or die('Wystąpił błąd' . mysqli_error($con));

        update_user_data($con);
    }