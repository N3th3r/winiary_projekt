<?php
	function check_for_record($link, $id)
	{
        $stmt = $link->prepare("SELECT COUNT(*) FROM users WHERE id=?");
        $stmt->bind_param('s', $id);
        $resp = $stmt->execute();
        $stmt->bind_result($resp);
        $stmt->fetch();
        $stmt->close();
        if($resp > 0) 
		{
			return True;
		} 
		else 
		{
			return False;
		}
	}

	function add_user($link) 
	{
		$user_id = $_POST["id"];
		$password_hash = password_hash($_POST["password"], PASSWORD_DEFAULT);

		if(!check_for_record($link, $user_id))
		{
			$stmt = $link->prepare("INSERT INTO users VALUES (?, ?, 1024)");
			$stmt->bind_param('ss', $user_id, $password_hash);
			$stmt->execute() or die('Wystąpił błąd' . mysqli_error($link)); 
			
			$join_date = date("Y-m-d");
			$img_blob = file_get_contents("../resources/images/avatar.jpg");
			$stmt = $link->prepare("INSERT INTO user_data VALUES (?, ?, '', ?)");
			$stmt->bind_param('sss', $user_id, $join_date, $img_blob);
			$stmt->execute() or die('Wystąpił błąd' . mysqli_error($link)); 
			header("Refresh:0; url=../signin.php?success=1");
		}
		else
		{
			header("Refresh:0; url=../signin.php?success=0");
		}
	}	

	$con = mysqli_connect('localhost', 'root', '');
    if(!$con) 
	{
        die('Wystąpił błąd' . mysqli_error($con));
    }
	
    $db = mysqli_select_db($con, 'fishing');
    if(!$db) 
	{
        die('Wystąpił błąd' . mysqli_error($con));
    }
	add_user($con);
	mysqli_close($con);

?>