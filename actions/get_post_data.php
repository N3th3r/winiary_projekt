<?php
    function get_post_data($id)    
    {
        $con = mysqli_connect('localhost', 'root', '') or die('Wystąpił błąd' . mysqli_error($con));        
        mysqli_select_db($con, 'fishing') or die('Wystąpił błąd' . mysqli_error($con));
        $stmt = $con->prepare("SELECT * FROM posts WHERE id=?");
        $stmt->bind_param('s', $id);
        $resp = $stmt->execute();
        $stmt->bind_result($id, $title, $text, $creation_time, $user_id, $section, $response_to, $thread_id);
        $stmt->fetch();
        $stmt->close();
        return array("id" => $id, "title" => $title, "text" => $text, "creation_time" => $creation_time, "user_id" => $user_id, "section" => $section, "response_to" => $response_to, "thread_id" => $thread_id);
    }
?>