<?php 
    function get_user_data($id)
    {
        $con = mysqli_connect('localhost', 'root', '') or die('Wystąpił błąd' . mysqli_error($con));        
        mysqli_select_db($con, 'fishing') or die('Wystąpił błąd' . mysqli_error($con));

        $stmt = $con->prepare("SELECT * FROM user_data WHERE id=?");
        $stmt->bind_param('s', $id);
        $stmt->execute();
        $stmt->bind_result($id, $join_date, $bio, $profile_img);
        $stmt->fetch();
        $stmt->close();
        mysqli_close($con);
        return array("join_date" => date("d-m-Y", strtotime($join_date)), "bio" => $bio, "profile_img" => "'data:image/jpg;base64," . base64_encode($profile_img) . "'");
    }
?>