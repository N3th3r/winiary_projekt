<?php
    function add_report($link, $post_id, $reporter_id)
    {
        $stmt = $link->prepare("INSERT INTO reported VALUES (DEFAULT, ?, ?, 'reserved')");
        $stmt->bind_param('is', $post_id, $reporter_id);
        $stmt->execute() or die('Wystąpił błąd' . mysqli_error($link)); 
    }
    if(isset($_GET["id"]))
    {
        session_start();
        if(isset($_SESSION["user"]))
        {
            $reporter_id = $_SESSION["user"]["id"];
            $previous_page = $_SERVER['HTTP_REFERER'];
            $con = mysqli_connect('localhost', 'root', '') or die('Wystąpił błąd' . mysqli_error($con));        
            mysqli_select_db($con, 'fishing') or die('Wystąpił błąd' . mysqli_error($con));    
            add_report($con, $_GET["id"], $reporter_id);
			header("Refresh:0; url=" . $previous_page);
        }
        else
        {
			header("Refresh:0; url=../login.php");
        }
    }
?>