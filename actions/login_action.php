<?php
	function verify_creds($link)
	{
		$user_id = $_POST['id'];
		$user_pass = $_POST['password'];
        $stmt = $link->prepare("SELECT * FROM users WHERE id=?");
        $stmt->bind_param('s', $user_id);
        $stmt->execute();
        $stmt->bind_result($id, $password_hash, $permissions);
        $stmt->fetch();
        $stmt->close();
		if(password_verify($user_pass, $password_hash))
		{
			$user_data = array('id' => $user_id, 'permissions' => $permissions);
			$_SESSION['user'] = $user_data;
			header("Refresh:0; url=../index.php");
		}
		else
		{
			header("Refresh:0; url=../login.php?w_pass=1");
		}
		
	}

	session_start();	
	$con = mysqli_connect('localhost', 'root', '') or die('Wystąpił błąd' . mysqli_error($con));	
    mysqli_select_db($con, 'fishing') or die('Wystąpił błąd' . mysqli_error($con));
	verify_creds($con);
	mysqli_close($con);

?>	