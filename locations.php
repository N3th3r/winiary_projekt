<?php
    include "actions/permissions.php";

    session_start();

    function render_location($id, $user_id, $mod_date, $title, $text)
    {
        ?>
            <div style="display: inline-block;">
                <div class="article_prev">
                    <div class="title">
                        <a href="location.php?id=<?php echo $id?>" style="line-height: 30px; text-decoration: none;"><?php echo $title ?></a>
                    </div>
                    <div class="thumbnail">
                        <img src="uploads/locations/<?php echo $id?>/thumbnail.jpg" class="thumbnail_img"/>
                    </div>
                    <div class="text">
                        <?php echo $text ?>
                    </div>
                </div>
            </div>
        <?php
    }
    function render_locations()
    {
        $con = mysqli_connect('localhost', 'root', '') or die('Wystąpił błąd' . mysqli_error($con));        
        mysqli_select_db($con, 'fishing') or die('Wystąpił błąd' . mysqli_error($con));            
        $stmt = $con->prepare("SELECT * FROM locations");
        $resp = $stmt->execute();
        $stmt->bind_result($id, $user_id, $mod_date, $title, $text, $maps_link);
        while($stmt->fetch())
        {
            render_location($id, $user_id, $mod_date, $title, $text);
        }
        $stmt->close();        
    }
?>
<html>
    <head>
        <link rel="stylesheet" href="resources/css/style.css">
        <title>Łowiska</title>
    </head>
    <body>
        <script src="scripts/nav.js"></script>
        <div id="nav_bar" class="nav_bar"></div>
        <div class="content">
            <h1>Łowiska</h1>
            <?php
                if(check_privileges("ALL", "Article_moderation"))
                {
                    ?>
                        <div class="controls">
                            <a href="edit_location.php" class="button">Dodaj wpis</a>
                        </div>
                    <?php
                }
                render_locations();
            ?>
        </div>    
    </body>
</html>