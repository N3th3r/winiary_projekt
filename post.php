<?php
        include "actions/get_post_data.php";
        include "actions/get_user_data.php";

        function render_comments($thread_data)
        {
            $con = mysqli_connect('localhost', 'root', '') or die('Wystąpił błąd' . mysqli_error($con));        
            mysqli_select_db($con, 'fishing') or die('Wystąpił błąd' . mysqli_error($con));            
            $stmt = $con->prepare("SELECT * FROM posts WHERE thread_id=?");
            $stmt->bind_param('i', $thread_data["id"]);
            $resp = $stmt->execute();
            $stmt->bind_result($id, $title, $text, $creation_time, $user_id, $section, $response_to, $thread_id);
            while($stmt->fetch())
            {
                $post_data = array("id" => $id, "title" => $title, "text" => $text, "creation_time" => $creation_time, "user_id" => $user_id, "section" => $section, "response_to" => $response_to, "thread_id" => $thread_id);
                render_post($post_data);
            }
            $stmt->close();
        }
        function render_post($post_data)
        {
            $user_data = get_user_data($post_data["user_id"]);
            ?>
            <div id="post_<?php echo $post_data['id']?>" class="post_show"> 
                <div name="header" class="header">
                    <span><?php echo date("H:i:s d-m-Y", strtotime($post_data["creation_time"])) ?> </span>
                    <span style="float: right;"><a href="actions/report.php?id=<?php echo $post_data["id"]?>">Zgłoś</a></span>
                </div>
                <div name="user" class="user">
                    <img src=<?php echo $user_data["profile_img"]?> class="user_image"/>                 
                    <div style="text-align: center">
                        <a href="account.php?id=<?php echo $post_data["user_id"] ?>" class="user_name"><?php echo $post_data["user_id"]; ?></a>
                    </div>
                </div>
                <div name="title" class="title">
                    <h2 class="title_text">
                        <?php 
                            if($post_data["response_to"] == -1)
                            {
                                echo $post_data["title"];
                            }
                            else
                            {
                                echo "<span onclick=scroll_to('post_" . $post_data["response_to"] . "')> Odpowiedź na " . get_post_data($post_data["response_to"])["user_id"] . "</span>";
                            }
                        ?>
                    </h2>
                </div>
                <div name="text" class="text">
                    <p><?php echo nl2br($post_data["text"])?></p>
                </div>
                <div name="attachments" class="attachments">
                        <?php
                            $attachments_path = "uploads/posts/" . $post_data["id"] . "/";
                            if(file_exists($attachments_path))
                            {
                                $attachments = array_diff(scandir($attachments_path), array("..", "."));
                                foreach($attachments as $attachment)
                                {
                                    ?>
                                        <figure style="float: left; text-align: center; margin: 0.2vw;">
                                            <a href=<?php echo $attachments_path . $attachment?>> <img src=<?php echo $attachments_path . $attachment?> style='witdh: 8vh; height: 8vh;'/></a>
                                            <figcaption style="font-size:80%; max-width: 15ch; text-overflow: ellipsis; overflow: hidden; white-space: nowrap; ">
                                                <?php echo $attachment ?>
                                            </figcaption>
                                        </figure>
                                    <?php
                                }
                            }
                        ?>
                </div>
                <div name="footer" class="footer">
                    <div style="float: right">
                        <input type="button" value="Odpowiedz" onclick="show('<?php echo $post_data['user_id']?>', <?php echo $post_data['thread_id'] ?>, <?php echo $post_data['id']?>)">
                    </div>
                </div>
            </div>
            <?php
        }

        session_start();
        if(isset($_GET["id"]))
        {
            $thread_data = get_post_data($_GET["id"]);
        }
    ?>
    
<html>
    <head>
        <link rel="stylesheet" href="resources/css/style.css">
        <title><?php echo $thread_data["title"];?></title>
        <script type="text/javascript" src="scripts/toggle_editor.js"></script>
        <script type="text/javascript" src="scripts/scroll_to.js"></script>
    </head>
    <body>
        <script src="scripts/nav.js"></script>
        <div id="nav_bar" class="nav_bar"></div>
        <div class="content">
            <a href="forum.php?section=<?php echo $thread_data['section']?>#post_<?php echo $thread_data['id']?>" class="button">Powrót</a>
            <?php
                render_comments($thread_data);

            ?>
            <form id="comment_form" method="post" name="comment_form" action = "actions/create_post_action.php?section=<?php echo $thread_data["section"]?>"enctype="multipart/form-data" style="margin-top: 10px;">
                <div id="comment_editor" class="comment_editor">
                    <div name="reciepient" class="header">
                        <span>Odpowiadasz </span><a id="comment_reciepient"></a>
                    </div>
                    <div name="text" class="text">
                        <textarea name="post_text" maxlength=20000 style="margin-left: auto; margin-right: auto; display: block; width: 95%; height: 100%;"></textarea>
                    </div>
                    <div name="controls" class="footer">
                        <div style="float: right;">
                            <input type="submit" value="Odpowiedz" id="submit_btn" class="button"/>
                            <input type="reset" value="Anuluj" id="cancel_btn" onclick="hide()" class="button"/>
                        </div>
                    </div>
                </div>
                <input type="text" id="post_title" name="post_title" style="pointer-events: none; visibility: hidden;">
            </form>
        </div> 
    </body>
</html>