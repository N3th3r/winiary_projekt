<?php
        include "actions/get_user_data.php";
        function check_for_record($link, $id)
        {
            $stmt = $link->prepare("SELECT COUNT(*) FROM sections WHERE id=?");
            $stmt->bind_param('s', $id);
            $resp = $stmt->execute();
            $stmt->bind_result($resp);
            $stmt->fetch();
            $stmt->close();
            if($resp > 0) 
            {
                return True;
            } 
            else 
            {
                return False;
            }
        }
        function render_section($link, $section)
        {
            $stmt = $link->prepare("SELECT * FROM posts WHERE section=? and response_to=-1 ORDER BY creation_time DESC");
            $stmt->bind_param('s', $section);
            $resp = $stmt->execute();
            $stmt->bind_result($id, $title, $text, $creation_time, $user_id, $section, $response_to, $thread_id);
            while($stmt->fetch())
            {
                $user_data = get_user_data($user_id);
                ?>
                <div id="post_<?php echo $id?>" class="post"> 
                    <div name="header" class="header">
                        <span><?php echo date("H:i:s d-m-Y", strtotime($creation_time)) ?> </span>
                        <span style="float: right;"><a href="actions/report.php?id=<?php echo $id?>">Zgłoś</a></span>
                    </div>
                    <div name="user" class="user">
                        <img src=<?php echo $user_data["profile_img"]?> class="user_image"/>                 
                        <div style="text-align: center">
                            <a href="account.php?id=<?php echo $user_id ?>" class="user_name"><?php echo $user_id; ?></a>
                        </div>    
                    </div>
                    <div name="title" class="title">
                        <a href="post.php?id=<?php echo $id ?>" ><h2 class="title_text"><?php echo $title ?></h2></a>
                    </div>
                    <div name="text" class="text">
                        <p><?php echo nl2br($text)?></p>
                    </div>
                    <div name="footer" class="footer">
                        <span>
                            <?php
                                $attachments_path = "uploads/posts/" . $id;
                                if(file_exists($attachments_path))
                                {
                                    ?>
                                    <div class="attachments_text">Załączniki: <?php echo implode(", ", array_diff(scandir($attachments_path), array("..", "."))); ?></div>
                                    <?php
                                }
                            ?>
                        </span>                                
                    </div>
                </div>
                <?php
            }
            $stmt->close();
        }
        function render_section_select($link)
        {
            $stmt = $link->prepare("SELECT * FROM sections");
            $resp = $stmt->execute();
            $stmt->bind_result($id, $perm_req);
            while($stmt->fetch())
            {
                ?> 
                <a href="forum.php?section=<?php echo $id ?>" class="button"><?php echo $id ?></a>
                <?php
            }
            $stmt->close();
        }

        session_start();
        $con = mysqli_connect('localhost', 'root', '') or die('Wystąpił błąd' . mysqli_error($con));
        mysqli_select_db($con, 'fishing') or die('Wystąpił błąd' . mysqli_error($con));
        
?>

<html>
    <head>
        <link rel="stylesheet" href="resources/css/style.css">
        <title>Forum</title>
    </head>
    <body>
        <script src="scripts/nav.js"></script>
        <div id="nav_bar" class="nav_bar"></div>
        <div class="content">
            <h1>Forum</h1>
                <?php
                    if(isset($_GET["section"]))
                    {
                        ?>
                        <div class="controls">
                           <a href="forum.php" class="button">Powrót</a>
                           <a href="create_post.php?section=<?php echo $_GET['section']?>" class="button">Utwórz wątek</a> 
                        </div>
                        <?php
                        if(check_for_record($con, $_GET["section"]))
                        {
                            render_section($con, $_GET["section"]);
                        }
                    }
                    else
                    {
                        ?>
                        <div class="controls">
                            <?php render_section_select($con); ?>
                        </div>
                        <?php
                    }
                ?>
        </div>
    </body>
</html>