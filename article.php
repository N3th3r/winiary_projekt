<?php
    include "actions/permissions.php";

    session_start();

    function render_images($id)
    {
        $attachments_path = "uploads/articles/" . $id . "/";
        if(file_exists($attachments_path))
        {
            $attachments = array_values(array_diff(scandir($attachments_path), array("..", ".")));
            ?>
            <div class="image_display">
                <div style="grid-column: 1; grid-row: 1">
                    <div class="main">
                        <div style="grid-column: 1 / 4; grid-row: 1; align-content: center; ">
                            <a href='<?php echo $attachments_path . "thumbnail.jpg"?>' id="main_image_ref">
                                <img id="main_image" src='<?php echo $attachments_path . "thumbnail.jpg"?>' class="image"/>
                            </a>
                        </div>
                        <div class="arrow_left" onclick='scrollImage(<?php echo json_encode($attachments_path)?>,  <?php echo json_encode($attachments)?>, -1)'>
                            <div class="arrow"></div>
                        </div>
                        <div class="arrow_right" onclick='scrollImage(<?php echo json_encode($attachments_path)?>,  <?php echo json_encode($attachments)?>, 1)'>
                            <div class="arrow"></div>
                        </div>
                    </div>
                </div>
                <div style="grid-column: 1; grid-row: 2; margin: auto;">
            <?php

            foreach($attachments as $attachment)
            {
                ?>
                    
                        <img id='<?php echo "select_" . $attachment?>' src='<?php echo $attachments_path . $attachment?>' class="image_prev" onclick='changeImage(<?php echo json_encode($attachments_path)?>,  <?php echo json_encode($attachment)?>)'/>
                <?php
            }
            ?>
                </div>
            </div>
            <?php
        }
    }

    if(isset($_GET["id"]))
    {
        $article_id = $_GET["id"];
        $con = mysqli_connect('localhost', 'root', '') or die('Wystąpił błąd' . mysqli_error($con));        
        mysqli_select_db($con, 'fishing') or die('Wystąpił błąd' . mysqli_error($con));            
        $stmt = $con->prepare("SELECT modified_by, mod_date, title, text FROM articles WHERE id=?");
        $stmt->bind_param('i', $article_id);
        $resp = $stmt->execute();
        $stmt->bind_result($user_id, $mod_date, $title, $text);
        $stmt->fetch();
    }
    else
    {
        header("Refresh:0; url=index.php");        
    }
?>
<html>
    <head>
        <link rel="stylesheet" href="resources/css/style.css">
        <script src="scripts/article_change_img.js"></script>
        <title><?php echo $title?></title>
    </head>
    <body>
        <script src="scripts/nav.js"></script>
        <div id="nav_bar" class="nav_bar"></div>
        <div class="content">
        <?php
            if(check_privileges("ALL", "Article_moderation"))
            {
                echo "<a href='edit_article.php?id=" . $_GET["id"] . "' class='button'>Edytuj</a>";
            }
            
            echo "<span style='float: right; margin-right: 50px'>Ostatnia modyfikacja " . $mod_date . " przez " . $user_id . "</span>";
        ?>
            <h1><?php echo $title;?></h1>
            <?php render_images($_GET["id"]) ?>
            <div style="word-wrap: break-word;">
                <p><?php echo nl2br($text)?></p>
            </div>
        </div>
    </body>
</html>