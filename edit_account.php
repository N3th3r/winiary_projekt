<?php
    include "actions/get_user_data.php";
                
    session_start();
    
    if(isset($_SESSION["user"]))
    {
        $user_data = get_user_data($_SESSION["user"]["id"]);
    }
    else
    {
        header("Refresh:0; url=index.php");
    }
?>

<html>
    <head>
        <link rel="stylesheet" href="resources/css/style.css">
        <title>Profil</title>
    </head>
    <body>
        <script src="scripts/nav.js"></script>
        <div id="nav_bar" class="nav_bar"></div>
        <div class="content">
            <h1>Profil</h1>
            <form action="actions/edit_account_action.php" method="post" name="edit_form" enctype="multipart/form-data">			
                <div class="profile_box">
                    <div style="grid-row: 1; grid-column: 1; ">
                        <img src=<?php echo $user_data["profile_img"]?> class="big_user_image"/>
                    </div>
                    <div style="grid-row: 2; grid-column: 1; text-align: center;">
                        <span>Prześlij nowego avatara(max 256x256px, 1MB): </span> <input type="file" name="profile_img_up" accept="image/png, image/jpg, image/jpeg, image/gif"><br>
                        <span class="user_name"><?php echo $_SESSION["user"]["id"];?></span><br>
                        <span>Data dołączenia: <?php echo $user_data["join_date"] ?></span>
                    </div>
                    <div style="grid-row: 3; grid-column: 1; margin-left: auto; margin-right: auto; width: 50%">
                        <textarea name="bio_edit" maxlength="2000" style="width: 100%; height: 40vh; overflow: visible; resize: none;"><?php echo $user_data["bio"]?></textarea>
                    </div>
                </div>
                <div id="controls" class="controls">
                    <input type="submit" value="Zapisz" name="save_btn" class="button"/> 
                    <input type="submit" value="Anuluj" name="cancel_btn" class="button"/> 
                </div>
            </form>
        </div>
    </body>
</html>
