<html>
    <head>
        <link rel="stylesheet" href="resources/css/style.css">
        <title>Świat Wędkarza</title>
    </head>
    <body>
        <?php
            session_start();
            if(isset($_GET['logout']))
            {
                unset($_SESSION['user']);
            }        
        ?>
        <script src="scripts/nav.js"></script>
        <div id="nav_bar" class="nav_bar"></div>
        <div class="content">
            <h1>Witaj w Świecie Wędkarza</h1>
            <h2>O nas</h2>
            <p>
                Świat Wędkarza to największa w Polsce społeczność poświęcona wędkarstwu.
                Na naszej stronie możesz znaleźć:
                <li>Atlas ryb - pomocny w identyfikacji okazów.</li>
                <li>Spis łowisk - pozwoli ci znaleźć lokację odpowiednią do twoich preferencji.</li>
                <li>Forum - dające możliwość dyskusji z innymi pasjonatami oraz poszerzanie swojej wiedzy</li>  
            </p>
            <h2>Przez wędkarzy, dla wędkarzy</h2>
            <p>
                W skład administracji wchodzą doświadczeni łowcy którzy dbają o jakość informacji zawartych na stronie.
                Przyjazna atmosfera i dokładna moderacja zapewni Ci unikalne doświadczenie i sprawi, że będziesz chciał
                aktywnie uczestniczyć w naszej społeczności i rozwijać swoją pasję.
            </p>
        </div>
    </body>
</html>