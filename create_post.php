<?php
        session_start();
        if(!isset($_SESSION["user"]))
        {
            header("Refresh: 0; url=login.php");
        }
?>

<html>
    <head>
        <link rel="stylesheet" href="resources/css/style.css">
        <title>Edytor Wątku</title>
    </head>
    <body>
        <script src="scripts/nav.js"></script>
        <div id="nav_bar" class="nav_bar"></div>
        <div id="editor" class="content">
		<form action="actions/create_post_action.php?section=<?php echo $_GET["section"]?>&response_to=-1&thread_id=-1" method="post" name="edit_form" enctype="multipart/form-data">			
            <div id="text_data">
                <p>Tytuł</p>
                <input type="text" name="post_title">
                <p>Treść</p>
                <textarea name="post_text" maxlength=50000, cols=80% rows=50%></textarea>
                <p>Załączniki(Maksymalnie 10)</p>
                <input type="file" name="attachments[]" multiple><br>
                

            </div>
            <br>
			<div id="controls" class="controls">
				<input type="submit" value="Utwórz" name="save_btn" class="button"/> 
				<input type="reset" value="Anuluj" name="cancel_btn" class="button" onClick="window.history.back();"/> 
			</div>
        </div>
    </body>
</html>