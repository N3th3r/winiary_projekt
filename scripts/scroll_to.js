function scroll_to(element_id)
{
    var element = document.getElementById(element_id);
    element.scrollIntoView({behavior: "smooth", block: "center", inline: "nearest"});
//    element.animate([{borderTop: "solid", borderTopColor: "yellow", borderTopWidth: "5px", borderBottom: "solid", borderBottomColor: "yellow", borderBottomWidth: "5px"},
//            {borderTop: "none", borderTopWidth: "0px", borderTopColor: "white", borderBottom: "none", borderBottomWidth: "0px", borderBottomColor: "white"}], 
//        {"duration" : 6000});
    element.animate([{backgroundColor: "#5fcc00"},
            {backgroundColor: "#bfff80"}], 
        {"duration" : 1500});


}