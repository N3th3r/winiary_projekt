var userDataReq = new XMLHttpRequest(); 
userDataReq.onload = function() {
    var userData = JSON.parse(this.responseText);
    var navStr = "";
    if(userData != null)
    {
        var tiles = [
            {ref: "index.php", name: "Strona Główna"},
            {ref: "forum.php", name: "Forum"},
            {ref: "atlas.php", name: "Atlas"},
            {ref: "locations.php", name: "Łowiska"},
            {ref: `account.php?id=${userData["raw"]["id"]}`, name: "Profil"},
            {ref: "index.php?logout=1", name: "Wyloguj"}
        ]
        if(userData["readable"].includes("Admin_panel"))
        {
            tiles.push({ref: "admin_panel.php", name: "Administracja"})
        }
    }
    else
    {
        var tiles = [
            {ref: "index.php", name: "Strona Główna"},
            {ref: "forum.php", name: "Forum"},
            {ref: "atlas.php", name: "Atlas"},
            {ref: "locations.php", name: "Łowiska"},
            {ref: "login.php", name: "Profil"},
            {ref: "login.php", name: "Zaloguj"}
        ]
    }
    for(const tile of tiles)
    {
        navStr += `<a href=${tile["ref"]} class="nav_button">${tile["name"]}</a> `;
    }
    document.getElementById("nav_bar").innerHTML = navStr;
};
userDataReq.open("get", "actions/get_session_data.php", true);
userDataReq.send();
