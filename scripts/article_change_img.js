function scrollImage(path, files, direction)
{
    var main_image = document.getElementById("main_image");
    var main_image_ref = document.getElementById("main_image_ref");
    var curr_img_index = files.indexOf(main_image.src.split(path)[1]);
    var next_img_index = curr_img_index + direction;
    if(next_img_index < 0)
    {
        next_img_index = files.length - 1;
    }
    else if(next_img_index >= files.length)
    {
        next_img_index = 0;
    }
    var file_path = `${path}${files[next_img_index]}`;
    main_image.src = file_path;
    main_image_ref.href = file_path;
}

function changeImage(path, file)
{
    var main_image = document.getElementById("main_image");
    var main_image_ref = document.getElementById("main_image_ref");
    var image_selection = document.getElementById(`select_${file}`);
    var file_path = `${path}${file}`;
    main_image.src = file_path;
    main_image_ref.href = file_path;
}