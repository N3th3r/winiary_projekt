function toggle_perm_edit(user_id, privileges)
{
    document.getElementById("permissions_form").reset();
    document.getElementById("permissions_form").action = `../actions/edit_permissions.php?id=${user_id}`;
    privileges.forEach(privilege => {
        document.getElementById(`${privilege}_checkbox`).checked = true;
    });
}