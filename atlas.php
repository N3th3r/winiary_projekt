<?php
    include "actions/permissions.php";

    session_start();

    function render_article($id, $user_id, $mod_date, $title, $text)
    {
        ?>
            <div style="display: inline-block;">
                <div class="article_prev">
                    <div class="title">
                        <a href="article.php?id=<?php echo $id?>" style="line-height: 30px; text-decoration: none;"><?php echo $title ?></a>
                    </div>
                    <div class="thumbnail">
                        <img src="uploads/articles/<?php echo $id?>/thumbnail.jpg" class="thumbnail_img"/>
                    </div>
                    <div class="text">
                        <?php echo $text ?>
                    </div>
                </div>
            </div>
        <?php
    }
    function render_articles()
    {
        $con = mysqli_connect('localhost', 'root', '') or die('Wystąpił błąd' . mysqli_error($con));        
        mysqli_select_db($con, 'fishing') or die('Wystąpił błąd' . mysqli_error($con));            
        $stmt = $con->prepare("SELECT * FROM articles");
        $resp = $stmt->execute();
        $stmt->bind_result($id, $user_id, $mod_date, $title, $text);
        while($stmt->fetch())
        {
            render_article($id, $user_id, $mod_date, $title, $text);
        }
        $stmt->close();        
    }
?>
<html>
    <head>
        <link rel="stylesheet" href="resources/css/style.css">
        <title>Atlas Ryb</title>
    </head>
    <body>
        <script src="scripts/nav.js"></script>
        <div id="nav_bar" class="nav_bar"></div>
        <div class="content">
            <h1>Atlas</h1>
            <?php
                if(check_privileges("ALL", "Article_moderation"))
                {
                    ?>
                        <div class="controls">
                            <a href="edit_article.php" class="button">Dodaj wpis</a>
                        </div>
                    <?php
                }
                render_articles();
            ?>
        </div>    
    </body>
</html>