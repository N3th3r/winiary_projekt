<html>
    <?php 
        include "actions/permissions.php";

        const PANELBINDING = array(
            "User_moderation" => array("name" => "Użytkownicy", "ref" => "panel/user_moderation.php"),
            "Forum_moderation" => array("name" => "Zgłoszone", "ref" => "panel/reported.php")
        );

        function render_panel()
        {
            $available_menus = array_intersect(array_keys(PANELBINDING), get_privileges());
        ?>
            <div id="panel" style="display: grid;  grid-template-columns: 250px auto; grid-template-rows: 100%; background-color:lightblue; witdh: 100%; height: 100%; margin-top: 50px;">
                <div name="nav" style="grid-column: 1; grid-row: 1; background-color: #bfff80;">
                <?php
                    foreach($available_menus as $menu)
                    {
                        ?>
                            <button class="button" style="display: block;" onclick="change_config('<?php echo (PANELBINDING[$menu]["ref"])?>')"><?php echo PANELBINDING[$menu]["name"]?></button> 
                        <?php
                    }
                ?>
                </div>
                <div name="configs" style="grid-column: 2; grid-row: 1;background-color:green">
                    <iframe id="config_window" style="width: 100%; height: 100%; background-color:white;" allowtransparency="false" frameBorder="0"></iframe>
                </div>
            </div>
        <?php    
        }
        session_start();
        if(!check_privileges("ALL", "Admin_panel"))
        {
			header("Refresh:0; url=index.php");
        }
    ?>

    <head>
        <link rel="stylesheet" href="resources/css/style.css">
        <script type="text/javascript" src="scripts/admin_panel.js"></script>    
        <title>Panel Administracyjny</title>
    </head>
    <body>
        <script src="scripts/nav.js"></script>
        <div id="nav_bar" class="nav_bar"></div>
        <div class="content">
            <?php render_panel() ?>
        </div>
    </body>
</html>