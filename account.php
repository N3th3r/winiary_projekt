<html>
    <?php
        include "actions/get_user_data.php";
                
        session_start();
        if(isset($_GET["id"]))
        {
            $user_data = get_user_data($_GET["id"]);
        }
        else
        {
            header("Refresh:0; url=index.php");
        }                    
    ?>
    <head>
        <link rel="stylesheet" href="resources/css/style.css">
        <title>Profil</title>
    </head>
    <body>
        <script src="scripts/nav.js"></script>
        <div id="nav_bar" class="nav_bar"></div>
        <div class="content">
            <h1>Profil</h1>

            <div class="profile_box">
                <div style="grid-row: 1; grid-column: 1; ">
                    <img src=<?php echo $user_data["profile_img"]?> class="big_user_image"/>
                </div>
                <div style="grid-row: 2; grid-column: 1; text-align: center;">
                    <span class="user_name"><?php echo $_GET["id"];?></span><br>
                    <span>Data dołączenia: <?php echo $user_data["join_date"] ?></span>
                </div>
                <div style="grid-row: 3; grid-column: 1; margin-left: auto; margin-right: auto; width: 50%; border: solid; border-color: #444422; background-color: #bfff80; margin-bottom: 25px;">
                    <p><?php echo nl2br($user_data["bio"]) ?></p>
                </div>

            </div>
            <?php
                if(isset($_SESSION["user"]) && $_SESSION["user"]["id"] == $_GET["id"])
                {
                    ?>
                    <div class="controls">
                        <a href=edit_account.php class='button'>Edytuj Profil</a>
                    </div>
                    <?php
                }
            ?>
        </div>
    </body>
</html>