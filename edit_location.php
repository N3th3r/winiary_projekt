<?php
    include "actions/permissions.php";

    session_start();

    $get_param = "";
    $title_value = "";
    $text_value = "";
    $maps_link = "";

    if(isset($_GET["id"]))
    {
        $get_param = "?id=" . $_GET["id"];
        $article_id = $_GET["id"];
        $con = mysqli_connect('localhost', 'root', '') or die('Wystąpił błąd' . mysqli_error($con));        
        mysqli_select_db($con, 'fishing') or die('Wystąpił błąd' . mysqli_error($con));            
        $stmt = $con->prepare("SELECT modified_by, mod_date, title, text, maps_link FROM locations WHERE id=?");
        $stmt->bind_param('i', $_GET["id"]);
        $resp = $stmt->execute();
        $stmt->bind_result($user_id, $mod_date, $title, $text, $maps_link);
        $stmt->fetch();

        $get_param = "?id=" . $_GET["id"];
        $title_value = $title;
        $text_value = $text;        
    }
    
    if(!check_privileges("ALL", "Article_moderation"))
    {
        header("Refresh:0; url=index.php");
    }
?>

<html>
    <head>
        <link rel="stylesheet" href="resources/css/style.css">
        <title>Edytor Artykułu</title>
    </head>
    <body>
        <script src="scripts/nav.js"></script>
        <div id="nav_bar" class="nav_bar"></div>
        <div id="editor" class="content">
		<form action="actions/edit_location_action.php<?php echo $get_param?>" method="post" name="edit_form" enctype="multipart/form-data">			
            <div id="text_data">
                <p>Tytuł</p>
                <input type="text" name="article_title" value=<?php echo  "'" . $title_value . "'"?>>
                <p>Link mapy</p>
                <input type="text" name="maps_link" value=<?php echo  "'" . $maps_link . "'"?>>
                <p>Miniatura</p>
                <input type="file" name="thumbnail"><br>
                <p>Treść</p>
                <textarea name="article_text" maxlength=65000, cols=100% rows=50%> <?php echo $text_value?></textarea>
                <p>Załączniki</p>
                <input type="file" name="attachments[]" multiple><br>
            </div>
            <br>
			<div id="controls" class="controls">
				<input type="submit" value="Utwórz" name="save_btn" class="button"/> 
				<input type="reset" value="Anuluj" name="cancel_btn" class="button" onClick="window.history.back();"/> 
			</div>
        </div>
    </body>
</html>