<html>
    <?php
        include "../actions/permissions.php";

        function render_report($report_id, $reporter_id, $report_comment, $post_id, $post_title, $post_text, $post_creation_time, $post_user_id, $post_section, $post_response_to, $post_thread_id)
        {
            echo "<tr>";
            echo "<td style='border: solid; border-collapse: collapse;'>$report_id</td>";
            echo "<td style='border: solid; border-collapse: collapse;'>$reporter_id</td>";
            echo "<td style='border: solid; border-collapse: collapse;'>$post_user_id</td>";
            echo "<td style='border: solid; border-collapse: collapse;'>$post_section</td>";
            echo "<td style='border: solid; border-collapse: collapse;'>$post_text</td>";
            echo "<td style='border: solid; border-collapse: collapse;'>$post_creation_time</td>";
            echo "<td style='border: solid; border-collapse: collapse;'><a href='../post.php?id=$post_thread_id#post_$post_id' target='_blank' class='button'>Pokaż</a><a href='../actions/remove_report.php?id=$post_id' class='button'>Zignoruj</a><a href='../actions/remove_post.php?id=$post_id' class='button'>Usuń</a></td>";
            echo "</tr>";

        }
        function render_reported()
        {
            $con = mysqli_connect('localhost', 'root', '') or die('Wystąpił błąd' . mysqli_error($con));        
            mysqli_select_db($con, 'fishing') or die('Wystąpił błąd' . mysqli_error($con));
    
            $stmt = $con->prepare("SELECT reported.id, reported.reporter_id, reported.report_comment, posts.id, posts.title, posts.text, posts.creation_time, posts.user_id, posts.section, posts.response_to, posts.thread_id FROM reported LEFT JOIN posts ON posts.id = reported.post_id") or die('Wystąpił błąd' . mysqli_error($con));
            $stmt->execute();
            $stmt->bind_result($report_id, $reporter_id, $report_comment, $post_id, $post_title, $post_text, $post_creation_time, $post_user_id, $post_section, $post_response_to, $post_thread_id);
            ?> 
            <table style="border: solid; border-collapse: collapse; width: 100%"> 
                <tr>
                    <th style="border: solid; border-collapse: collapse;">ID zgłoszenia</th>
                    <th style="border: solid; border-collapse: collapse;">Zgłaszający</th>
                    <th style="border: solid; border-collapse: collapse;">Autor</th>
                    <th style="border: solid; border-collapse: collapse;">Sekcja</th>
                    <th style="border: solid; border-collapse: collapse;">Zawartość</th>
                    <th style="border: solid; border-collapse: collapse;">Data utworzenia</th>
                    <th style="border: solid; border-collapse: collapse;">Opcje</th>
                </tr>
            <?php
            while($stmt->fetch())
            {

                render_report($report_id, $reporter_id, $report_comment, $post_id, $post_title, $post_text, $post_creation_time, $post_user_id, $post_section, $post_response_to, $post_thread_id);
            }
            ?> </table> <?php
            $stmt->close();
            mysqli_close($con);
        }

        session_start();
        if(!check_privileges("ALL", "Admin_panel", "Forum_moderation"))
        {
			header("Refresh:0; url=index.php");
        }
    ?>
    <head>
        <link rel="stylesheet" href="../resources/css/style.css">   
        <title>Atlas Ryb</title>
    </head>
    <body style="background-color:white; background-image:none;">
        <div class="background-color: white; ">
            <?php render_reported() ?>
        </div>
    </body>
</html>