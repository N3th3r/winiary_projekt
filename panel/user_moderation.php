<html>
    <?php
        include "../actions/permissions.php";

        function render_permission_edit()
        {
            ?>
                <form method="post" name="permissions_form" id="permissions_form">
            <?php
            foreach(array_keys(PRIVILAGE_LIST) as $privilege)
            {
                ?>
                    <input type="checkbox" id="<?php echo $privilege?>_checkbox" name="set_privileges[]" value=<?php echo $privilege?>>
                    <label for="<?php echo $privilege?>_checkbox"><?php echo $privilege?></label>
                <?php

            }
            ?>
                    <input type="submit" value="Ustaw" id="submit_btn"/> 
                </form>
            <?php
        }

        function render_user($user_id, $user_permissions, $user_join_date)
        {
            echo "<tr>";
            echo "<td style='border: solid; border-collapse: collapse;'>$user_id</td>";
            echo "<td style='border: solid; border-collapse: collapse;'>$user_join_date</td>";
            echo "<td style='border: solid; border-collapse: collapse;'>";
            if(check_privileges('ALL', 'Privileges_edit'))
            {
                echo "<button onclick='toggle_perm_edit(" . json_encode($user_id) . ", " . json_encode(translate_permission_sum($user_permissions)) . ")'>Modyfikuj Uprawnienia</button>";
            }
            echo "<a href='actions/remove_user.php'>Usuń<a></td>";
            echo "</tr>";

        }

        function render_users()
        {
            $con = mysqli_connect('localhost', 'root', '') or die('Wystąpił błąd' . mysqli_error($con));        
            mysqli_select_db($con, 'fishing') or die('Wystąpił błąd' . mysqli_error($con));
    
            $stmt = $con->prepare("SELECT users.id, users.permissions, user_data.join_date FROM users LEFT JOIN user_data ON users.id = user_data.id");
            $stmt->execute();
            $stmt->bind_result($user_id, $user_permissions, $user_join_date);
            ?> 
            <table style="border: solid; border-collapse: collapse; width: 100%"> 
                <tr>
                    <th style="border: solid; border-collapse: collapse;">ID użytkownika</th>
                    <th style="border: solid; border-collapse: collapse;">Data dołączenia</th>
                    <th style="border: solid; border-collapse: collapse;">Opcje</th>
                </tr>
            <?php
            while($stmt->fetch())
            {
                render_user($user_id, $user_permissions, $user_join_date);
            }
            ?> </table> <?php
            $stmt->close();
            mysqli_close($con);
        }

        session_start();
        if(!check_privileges("ALL", "Admin_panel", "User_moderation"))
        {
			header("Refresh:0; url=index.php");
        }
    ?>
    <head>
        <link rel="stylesheet" href="../resources/css/style.css">
        <script type="text/javascript" src="../scripts/toggle_perm_edit.js"></script>    
        <title>Atlas Ryb</title>
    </head>
    <body style="background-color:white; background-image:none;">
        <div class="background-color: white; ">
            <?php 
                render_users();
                render_permission_edit(); 
            ?>
        </div>
    </body>
</html>